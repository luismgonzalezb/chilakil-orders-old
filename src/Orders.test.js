/* global it */
/* eslint react/jsx-filename-extension: 0 */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Orders from './Orders';
import createStore from './data/store';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const store = createStore();
  ReactDOM.render(
    <Provider store={store}>
      <Orders />
    </Provider>,
    div,
  );
});
