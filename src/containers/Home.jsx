import React, { Component } from 'react';
import { connect } from 'react-redux';

class Home extends Component {
  componentDidMount() {
  }
  render() {
    return (
      <div>
        <h1>Home</h1>
      </div>
    );
  }
}

Home.propTypes = {
};

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {
})(Home);
