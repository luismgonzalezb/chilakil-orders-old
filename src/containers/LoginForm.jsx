import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Login from '../components/Login';
import { login } from '../data/user/user-actions';

class LoginForm extends Component {
  componentDidMount() {
  }
  handleLogin = values => this.props.login(values)
  render() {
    return (
      <Login onSubmit={this.handleLogin} />
    );
  }
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {
  login,
})(LoginForm);
