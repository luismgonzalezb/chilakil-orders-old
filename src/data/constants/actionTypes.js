// CONFIG
export const FETCHING = 'FETCHING';
export const ON_ERROR = 'ON_ERROR';
// MENU
export const GET_ALL_ITEMS = 'GET_ALL_ITEMS';
// USER
export const LOGIN = 'LOGIN';
