import { applyMiddleware, createStore, combineReducers } from 'redux';
import { routerReducer as router, routerMiddleware } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger';
import user from '../data/user/user-reducer';

const reducers = combineReducers({
  form,
  user,
  router,
});

export default (initialState = {}, history) => {
  const rmiddleware = routerMiddleware(history);
  const middleware = applyMiddleware(ReduxThunk, logger, rmiddleware);
  const store = middleware(createStore)(reducers, initialState);
  return store;
};
