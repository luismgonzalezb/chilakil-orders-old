import { LOGIN } from '../constants/endpoints';
import docCookies from './cookies';
// login callback
const loginCallback = (response, resolve, reject) => {
  if (response.status === 'PARTIALLY_AUTHENTICATED') {
    const { code, state } = response;
    // Send code to server to exchange for access token
    fetch(LOGIN, {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ accountKitCode: code, csrf: state }),
    }).then((resp) => {
      if (resp.status === 200 && resp.statusText === 'OK') {
        return resolve({ loggedIn: true });
      }
      throw resp;
    }).catch(() => reject('SERVER ERROR'));
  } else {
    reject(response.status);
  }
};

export const emailLogin = (callback, emailAddress) => window.AccountKit.login(
  'EMAIL',
  { emailAddress }, callback,
);

export const smsLogin = (callback, phoneNumber, countryCode = '+1') => window.AccountKit.login(
  'PHONE',
  { countryCode, phoneNumber }, callback,
);

export const login = values => new Promise((resolve, reject) => {
  const cb = response => loginCallback(response, resolve, reject);
  return values.phone ? smsLogin(cb, values.phone) : emailLogin(cb, values.email);
});

export const isLoggedIn = () => docCookies.hasItem('token') && docCookies.hasItem('chuser');
