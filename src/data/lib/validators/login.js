
export default (values) => {
  const errors = {};
  if (!values.email && !values.phone) {
    errors.email = 'Required';
    errors.phone = 'Required';
  }
  return errors;
};
