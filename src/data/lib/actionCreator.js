export default (type, argNames = []) => (args) => {
  const action = { type };
  if (argNames.length) {
    for (let i = 0; i < argNames.length; i += 1) {
      const name = argNames[i];
      action[name] = args[name];
    }
  } else {
    action.data = args;
  }
  return action;
};
