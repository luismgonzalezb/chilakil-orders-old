import {
  FETCHING,
  ON_ERROR,
} from '../constants/actionTypes';

const onFetchReducer = (state, action) =>
  Object.assign({}, state, { loading: action.data });

const onErrorReducer = (state, action) =>
  Object.assign({}, state, { error: action.data });

const ACTION_HANDLER = {
  [FETCHING]: onFetchReducer,
  [ON_ERROR]: onErrorReducer,
};

export default (state = {}, action) => {
  const handler = ACTION_HANDLER[action.type];
  return handler ? handler(state, action) : state;
};
