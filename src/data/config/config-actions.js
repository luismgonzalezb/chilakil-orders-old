import actionCreator from '../lib/actionCreator';
import {
  ON_ERROR,
  FETCHING,
} from '../constants/actionTypes';

export const onFetchAction = actionCreator(FETCHING);
export const onError = actionCreator(ON_ERROR);
