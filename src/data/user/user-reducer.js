import { LOGIN } from '../constants/actionTypes';

const loginReducer = (state, action) =>
  Object.assign({}, state, action.user);

const ACTION_HANDLER = {
  [LOGIN]: loginReducer,
};

export default (state = {}, action) => {
  const handler = ACTION_HANDLER[action.type];
  return handler ? handler(state, action) : state;
};
