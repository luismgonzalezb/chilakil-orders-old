import actionCreator from '../lib/actionCreator';
import { LOGIN } from '../constants/actionTypes';
import { login as FBLogin } from '../lib/accountKit';
import { onFetchAction, onError } from '../config/config-actions';

const loginAction = actionCreator(LOGIN, ['user']);

export const login = auth => (dispatch) => {
  dispatch(onFetchAction(true));
  return FBLogin(auth).then((user) => {
    dispatch(onFetchAction(false));
    dispatch(loginAction({ user }));
  }).catch((err) => {
    dispatch(onFetchAction(false));
    dispatch(onError(err));
  });
};

export default login;
