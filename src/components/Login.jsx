import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import Button from 'material-ui/Button';
import InputField from '../components/form/InputField';
import validate from '../data/lib/validators/login';

import './Login.css';

const Login = ({ handleSubmit }) => (
  <div className="login hero is-fullheight">
    <div className="hero-body">
      <div className="container">
        <div className="columns is-centered">
          <div>
            <h1>Chilakil Takeria</h1>
            <form onSubmit={handleSubmit} className="login-form">
              <Field label="Phone" name="phone" type="phone" component={InputField} />
              <div className="normal-vert-margin fancy"><span>Or</span></div>
              <Field label="Email" name="email" type="email" component={InputField} />
              <Button raised type="submit" color="primary" className="normal-vert-margin">
                Login
              </Button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
);

Login.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'login',
  validate,
})(Login);
