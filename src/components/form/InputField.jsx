import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';

const InputField = ({
  input, label, margin, required, classNames, meta: { touched, error },
}) => (
  <TextField
    {...input}
    label={label}
    margin={margin}
    required={required}
    className={classNames}
    error={!!(touched && error)}
  />
);

InputField.defaultProps = {
  label: '',
  classNames: '',
  margin: 'normal',
  required: false,
};

InputField.propTypes = {
  label: PropTypes.string,
  margin: PropTypes.string,
  required: PropTypes.bool,
  classNames: PropTypes.string,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default InputField;
