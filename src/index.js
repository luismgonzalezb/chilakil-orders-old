/* eslint react/jsx-filename-extension: 0 */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import registerServiceWorker from './data/lib/registerServiceWorker';
import Orders from './Orders';
import createStore from './data/store';
import { isLoggedIn } from './data/lib/accountKit';
import './index.css';

const history = createHistory();
const store = createStore({ user: { loggedIn: isLoggedIn() } }, history);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Orders />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();
