import React from 'react';
import {
  Route,
  BrowserRouter as Router,
} from 'react-router-dom';
import Button from 'material-ui/Button';
import Home from './containers/Home';
import Menu from './containers/Menu';

const OrdersMain = () => (
  <Router>
    <div className="Orders">
      <div>
        <Button raised type="submit" color="primary" className="normal-vert-margin">
          New Order
        </Button>
      </div>
      <Route exact path="/" component={Home} />
      <Route path="/menu" component={Menu} />
    </div>
  </Router>
);

export default OrdersMain;
