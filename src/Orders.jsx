import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import LoginForm from './containers/LoginForm';
import OrdersMain from './OrdersMain';

import './Orders.css';

const Orders = ({ user: { loggedIn } }) => (
  loggedIn ? (
    <OrdersMain />
  ) : (
    <Route path="/" component={LoginForm} />
  )
);

Orders.propTypes = {
  user: PropTypes.object.isRequired,
};

const mapStateToProps = ({ user }) => ({
  user,
});

export default connect(mapStateToProps, {
})(Orders);
